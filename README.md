## 動作環境のチェック
VS2013 x86 Native Tools コマンド プロンプトを起動し、以下のコマンドを入力する
```
where cl
where root
where git
```
`情報: 与えられたパターンのファイルが見つかりませんでした。`と出なければ環境は整備されている
## 動作環境
* Windows PC 7以降
* Visual Studio 2013 https://support.microsoft.com/ja-jp/kb/3021976 の「最新の Visual Studio Community 2013 パッケージ」をダウンロード
* ROOT v5.34.32 vc12 https://root.cern.ch/download/root_v5.34.32.win32.vc12.exe からダウンロード
* Git for Windows https://git-for-windows.github.io/ からダウンロード

## 初心者用の使い方
次のコマンドを実行する
```
cd %USERPROFILE%
git clone --recursive https://gitlab.com/escan/PlotTools.git
cd PlotTools
root GRAINE2015\plot_dc.c() -l -q -b
sample\dc-002.pdf
``` 
これで、plot_dcのサンプル用グラフを作成できる。PDFファイルが表示されただろうか。