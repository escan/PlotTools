#ifdef __CINT__
//#include <stdint.h>
void logon();
#include "../root_macros/cern_root5_mystyle.hpp"
#define DBL_MAX         1.7976931348623158e+308 /* max value */
#elif _WIN32
#include "root_macro.hpp"
#endif

#include <exception>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>
#include <map>
#include <cmath>

#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TArrow.h>
#include <TPaveText.h>
#include <TGraph2D.h>
#include <TH1F.h>
#include <TMultiGraph.h>
#include <TTree.h>

#ifdef __CINT__
std::string to_string(int i) {
	std::stringstream ss;
	ss << i;
	return ss.str();
}
double hypot(double x, double y) {
	return sqrt(x*x + y*y);
}
#endif

using namespace std;

class CorrectionMap {
public:
	double x_min, x_max, y_min, y_max, x, y;
	double dax, day, shr;
	double da;
	double signal, bg, sn;
	double dax_width, day_width, shr_width;
	const int mname_id(std::string mname_id)const {
		if (mname_id == "dax") { return 1; }
		if (mname_id == "day") { return 2; }
		if (mname_id == "shr") { return 3; }
		if (mname_id == "da") { return 10; }
		if (mname_id == "signal") { return 104; }
		if (mname_id == "bg") { return 105; }
		if (mname_id == "dax_width") { return 1001; }
		if (mname_id == "day_width") { return 1002; }
		if (mname_id == "shr_width") { return 1003; }
	}
	const double& operator[](int mname_id)const {
		if (mname_id == 1) { return dax; }
		if (mname_id == 2) { return day; }
		if (mname_id == 3) { return shr; }
		if (mname_id == 10) { return da; }
		if (mname_id == 104) { return signal; }
		if (mname_id == 105) { return bg; }
		if (mname_id == 1001) { return dax_width; }
		if (mname_id == 1002) { return day_width; }
		if (mname_id == 1003) { return shr_width; }
		throw std::exception();
	}
	std::string object()const { return "x_min/D:x_max:y_min:y_max:x:y:dax:day:shr:da:signal:bg:sg:dax_width:day_width:shr_width"; }
	std::string name()const { return "CorrectionMap"; }
};


void plot_1Dhistgram(TCanvas &c1, string title, CorrectionMap &map, TTree &corrmap, int subpadnumber, double xlow, double xup, int layer, int mname_id) {
	c1.cd(subpadnumber);
	TH1F * hdist1 = new TH1F(title.c_str(), title.c_str(), 100, xlow, xup);
	hdist1->SetFillColor(kRed);
	for (int i = 0; i < corrmap.GetEntries() / 2; i++) {
		corrmap.GetEntry(i * 2 + layer);
		hdist1->Fill(map[mname_id]);
	}
	hdist1->Draw();
}
void plot_2Dhistgram(TCanvas &c1, string title, CorrectionMap &map, TTree &corrmap, int subpadnumber, int layer, int mname_id, double meanx, double meany, bool fixzrange = false, double xlow = 0, double xup = 0) {

	c1.cd(subpadnumber);

	TGraph2D * dmydist = new TGraph2D();
	dmydist->SetPoint(0, meanx - 65, meany - 50, 0);//xlow
	dmydist->SetPoint(1, meanx + 65, meany + 50, 1);//xup
	dmydist->SetMarkerColor(0);
	dmydist->GetXaxis()->SetTitle("X [mm]");
	dmydist->GetYaxis()->SetTitle("Y [mm]");
	dmydist->GetXaxis()->SetTitleOffset(1.5);
	dmydist->GetYaxis()->SetTitleOffset(1.5);

	c1.GetPad(subpadnumber)->SetTheta(90);
	c1.GetPad(subpadnumber)->SetPhi(1.e-14);

	dmydist->SetTitle((title).c_str());
	dmydist->Draw("PCOL");


	TGraph2D * gdist1 = new TGraph2D();
	vector<double> vec;
	for (int i = 0; i < corrmap.GetEntries(); i++) {
		if (i % 2 != layer) { continue; }
		corrmap.GetEntry(i);
		gdist1->SetPoint(i, map.x, map.y, map[mname_id]);
		vec.push_back(map[mname_id]);
	}
	double ave = std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
	gdist1->SetMarkerStyle(21);
	gdist1->SetMarkerSize(0.5);
	if (fixzrange)
	{
		gdist1->SetMaximum(xup);
		gdist1->SetMinimum(xlow);
	}
	else
	{
		//gdist1->SetMaximum(ave + width);
		//gdist1->SetMinimum(ave - width);
	}
	gdist1->Draw("PCOLZsame");
}
void plot_2D1Dhistgram(TCanvas &c1, string title, CorrectionMap &map, TTree &corrmap, double xlow, double xup, int mname_id, double meanx, double meany, bool fixzrange, double zwidth, string xaxis_title = "") {
	vector<TGraph2D*> dmydist;
	for (int j = 0; j < 2; j++)
	{
		TGraph2D * dmydist1 = new TGraph2D();
		dmydist1->SetPoint(0, meanx - 65, meany - 50, 0);//xlow
		dmydist1->SetPoint(1, meanx + 65, meany + 50, 1);//xup
		dmydist1->SetMarkerColor(0);
		dmydist1->GetXaxis()->SetTitle("X [mm]");
		dmydist1->GetYaxis()->SetTitle("Y [mm]");
		dmydist1->GetXaxis()->SetTitleOffset(1.5);
		dmydist1->GetYaxis()->SetTitleOffset(1.5);
		dmydist.push_back(dmydist1);
	}
	c1.Clear();
	c1.Divide(2, 2);

	vector<TH1F *> vhdist1;
	for (int layer = 0; layer < 2; layer++)
	{
		c1.cd(3 + layer);
		TH1F * hdist1 = new TH1F(("hdist" + to_string(layer + 1)).c_str(), (title + to_string(layer + 1)).c_str(), 100, xlow, xup);
		hdist1->GetXaxis()->SetTitle(xaxis_title.c_str());
		hdist1->SetFillColor(kRed);
		for (int i = 0; i < corrmap.GetEntries(); i++) {
			if (i % 2 != layer) { continue; }
			corrmap.GetEntry(i);
			hdist1->Fill(map[mname_id]);
		}
		hdist1->Draw();
		vhdist1.push_back(hdist1);

		double ave = hdist1->GetMean();

		c1.cd(1 + layer);
		c1.GetPad(1 + layer)->SetTheta(90);
		c1.GetPad(1 + layer)->SetPhi(1.e-14);

		dmydist[layer]->SetTitle((title + to_string(layer + 1)).c_str());
		dmydist[layer]->Draw("PCOL");

		TGraph2D * gdist1 = new TGraph2D();
		if (fixzrange)
		{
			gdist1->SetMaximum(ave + zwidth / 2);
			gdist1->SetMinimum(ave - zwidth / 2);
		}

		for (int i = 0; i < corrmap.GetEntries(); i++) {
			if (i % 2 != layer) { continue; }
			corrmap.GetEntry(i);
			gdist1->SetPoint(i, map.x, map.y, map[mname_id]);
		}
		gdist1->SetMarkerStyle(21);
		gdist1->SetMarkerSize(0.5);

		gdist1->Draw("PCOLZsame");
	}
}
void plot_Graph(TCanvas &c1, string title, CorrectionMap &map, TTree &corrmap, int subpadnumber, int layer, int mname_id1, int mname_id2, bool fixxyrange = false, double xlow = 0, double xup = 0, double ylow = 0, double yup = 0) {
	c1.cd(subpadnumber);

	vector<double> xvec, yvec;
	for (int i = 0; i < corrmap.GetEntries(); i++) {
		if (i % 2 != layer) { continue; }
		corrmap.GetEntry(i);
		xvec.push_back(map[mname_id1]);
		yvec.push_back(map[mname_id2]);
	}
	TGraph * mygraph = new TGraph(xvec.size(), &xvec[0], &yvec[0]);
	mygraph->SetTitle(title.c_str());
	if (fixxyrange) {
		mygraph->GetXaxis()->SetLimits(xlow, xup);
		mygraph->GetYaxis()->SetLimits(ylow, yup);
	}
	mygraph->Draw("AP");
}
void plot_distortion(TCanvas &c1, string fname, CorrectionMap &map, TTree &corrmap, int layer, double meanx, double meany, double extz) {
	TGraph* disdmy1 = new TGraph();
	disdmy1->SetPoint(0, meanx - 65, meany - 50);//xlow
	disdmy1->SetPoint(1, meanx + 65, meany + 50);//xup
	disdmy1->GetXaxis()->SetTitle("X [mm]");
	((TGaxis*)disdmy1->GetXaxis())->SetMaxDigits(3);

	disdmy1->GetYaxis()->SetTitle("Y [mm]");
	((TGaxis*)disdmy1->GetYaxis())->SetMaxDigits(3);
	disdmy1->SetTitle(("Distortion " + std::to_string(layer + 1) + " (" + fname + ")").c_str());
	disdmy1->GetXaxis()->SetLimits(meanx - 75, meanx + 75);
	disdmy1->GetYaxis()->SetLimits(meany - 60, meany + 60);

	c1.cd();
	disdmy1->Draw("AP");

	for (int i = 0; i < corrmap.GetEntries(); i++) {
		if (i % 2 != layer) { continue; }
		corrmap.GetEntry(i);
		TArrow *arrow = new TArrow(map.x, map.y, map.x + map.dax*extz, map.y + map.day*extz, 0.01, ">");
		arrow->SetFillColor(layer == 0 ? kRed : kBlue);
		arrow->SetLineColor(layer == 0 ? kRed : kBlue);
		arrow->SetFillStyle(1001);
		arrow->SetLineWidth(1);
		arrow->SetLineStyle(1);
		c1.cd();
		arrow->Draw();
	}
	//Angle refoerence
	double dax = 0.050;
	double day = 0;
	TArrow *arrow;
	arrow = new TArrow(meanx - 89, meany - 69, meanx - 89 + dax*extz, meany - 69 + day*extz, 0.01, ">");
	arrow->SetFillColor(1);
	arrow->SetLineColor(1);
	arrow->SetFillStyle(1001);
	arrow->SetLineWidth(2);
	arrow->SetLineStyle(1);
	c1.cd();
	arrow->Draw();
	dax = 0;
	day = 0.050;
	arrow = new TArrow(meanx - 89, meany - 69, meanx - 89 + dax*extz, meany - 69 + day*extz, 0.01, ">");
	arrow->SetFillColor(1);
	arrow->SetLineColor(1);
	arrow->SetFillStyle(1001);
	arrow->SetLineWidth(2);
	arrow->SetLineStyle(1);
	c1.cd();
	arrow->Draw();
	TPaveText *pt = new TPaveText(meanx - 90, meany - 78, meanx - 70, meany - 70, "br");
	pt->SetFillColor(0);
	pt->SetLineColor(0);
	pt->SetShadowColor(0);
	TText *text = pt->AddText("50 mrad");
	c1.cd();
	pt->Draw();

}
void read_correctionmap(string input_fname, CorrectionMap &map, TTree &corrmap) {
	ifstream data(input_fname.c_str());
	if (!data) { throw std::exception(("cannot open file. " + input_fname).c_str()); }

	const int Nmember = 41;
	double tmp[Nmember * 2];
	while (true)
	{
		for (int i = 0; i < 2; i++)
		{
			string str;
			std::getline(data, str);
			stringstream ss(str);
			for (int j = 0; j < Nmember; j++) {
				ss >> tmp[j + Nmember * i];
			}
		}
		if (data.eof()) { break; }
		for (int i = 0; i < 2; i++)
		{
			map.x_min = tmp[3 + i * Nmember] / 1000;
			map.x_max = tmp[4 + i * Nmember] / 1000;
			map.x = (map.x_min + map.x_max) / 2;
			map.y_min = tmp[5 + i * Nmember] / 1000;
			map.y_max = tmp[6 + i * Nmember] / 1000;
			map.y = (map.y_min + map.y_max) / 2;
			map.shr = tmp[13 + i * Nmember];
			map.dax = tmp[17 + i * Nmember];
			map.day = tmp[18 + i * Nmember];
			map.signal = tmp[20 + i * Nmember];
			map.bg = tmp[21 + i * Nmember];
			map.sn = tmp[22 + i * Nmember];
			map.da = std::hypot(map.dax, map.day);
			map.shr_width = tmp[38 + i * Nmember];
			map.dax_width = tmp[39 + i * Nmember];
			map.day_width = tmp[40 + i * Nmember];
			corrmap.Fill();
		}
	}
}

//root plot_dc.cpp+(\"dc-002.lst\",\"dc-002.pdf\") -l -b -q
//root plot_dc.cpp+(\"dc-002.lst\",\"dc-002.ps\") -l -b -q


int plot_dc(string input_fname, string output_fname)
{
#ifdef __CINT__
	SetMyStyle(true, true);
#endif

	TCanvas * c1 = new TCanvas("c1");
	c1->cd();
	c1->SaveAs((output_fname + "[").c_str());

	gStyle->SetNumberContours();

	try {
		//Extrapolated Z
		float extz = 250 * 1;

		gStyle->SetOptStat("emrou");

		TTree *corrmap = new TTree("name", "title");
		auto map = new CorrectionMap();
		corrmap->Branch(map->name().c_str(), map, map->object().c_str());

		read_correctionmap(input_fname, *map, *corrmap);
		corrmap->SaveAs();

		//MeanX MeanY
		double max_x = -DBL_MAX;
		double max_y = -DBL_MAX;
		double min_x = DBL_MAX;
		double min_y = DBL_MAX;
		for (int i = 0; i < corrmap->GetEntries(); i++) {
			corrmap->GetEntry(i);
			max_x = std::max(max_x, map->x);
			max_y = std::max(max_y, map->y);
			min_x = std::min(min_x, map->x);
			min_y = std::min(min_y, map->y);
		}
		double meanx = (max_x + min_x) / 2;
		double meany = (max_y + min_y) / 2;

		int layer = 0;

		//Distortion Arrow
		plot_distortion(*c1, input_fname, *map, *corrmap, 0, meanx, meany, extz);
		c1->SaveAs(output_fname.c_str());
		plot_distortion(*c1, input_fname, *map, *corrmap, 1, meanx, meany, extz);
		c1->SaveAs(output_fname.c_str());

		//Distortion Map Histo
		plot_2D1Dhistgram(*c1, "Distortion(abs.) ", *map, *corrmap, 0, 0.2, map->mname_id("da"), meanx, meany, true, 0.1, "#sqrt{dax1^{2}+day1^{2}}");
		c1->SaveAs(output_fname.c_str());

		//Shrink Map Histo
		plot_2D1Dhistgram(*c1, "Shrink ", *map, *corrmap, 0.5, 2, map->mname_id("shr"), meanx, meany, true, 0.2);
		c1->SaveAs(output_fname.c_str());

		//Signal, BG
		c1->Clear();
		c1->Divide(2, 2);

		plot_2Dhistgram(*c1, "Signal 1", *map, *corrmap, 1, 0, map->mname_id("signal"), meanx, meany);
		plot_2Dhistgram(*c1, "BG 1", *map, *corrmap, 2, 0, map->mname_id("bg"), meanx, meany);

		plot_1Dhistgram(*c1, "Signal 1", *map, *corrmap, 3, 0, 3000, 0, map->mname_id("signal"));
		plot_1Dhistgram(*c1, "BG 1", *map, *corrmap, 4, 0, 10, 0, map->mname_id("bg"));
		c1->SaveAs(output_fname.c_str());

		c1->Clear();
		c1->Divide(2, 2);
		plot_Graph(*c1, "Distortion(abs.) 1 vs # of trk", *map, *corrmap, 1, 0, map->mname_id("signal"), map->mname_id("da"));
		plot_Graph(*c1, "Shrink 1 vs # of trk", *map, *corrmap, 2, 0, map->mname_id("signal"), map->mname_id("shr"));
		plot_Graph(*c1, "Distortion(abs.) 2 vs # of trk", *map, *corrmap, 3, 1, map->mname_id("signal"), map->mname_id("da"));
		plot_Graph(*c1, "Shrink 2 vs # of trk", *map, *corrmap, 4, 1, map->mname_id("signal"), map->mname_id("shr"));
		c1->SaveAs(output_fname.c_str());

		c1->Clear();
		c1->Divide(2, 2);

		plot_1Dhistgram(*c1, "shrink-peak-flat-top-width", *map, *corrmap, 1, 0, 0.005, 0, map->mname_id("shr_width"));
		plot_1Dhistgram(*c1, "distortion-x-peak-flat-top-width", *map, *corrmap, 3, 0, 0.005, 0, map->mname_id("dax_width"));
		plot_1Dhistgram(*c1, "distortion-y-peak-flat-top-width", *map, *corrmap, 4, 0, 0.005, 0, map->mname_id("day_width"));
		c1->SaveAs(output_fname.c_str());
	}
	catch (std::exception &ex) {
		cout << ex.what() << endl;
	}
	c1->SaveAs((output_fname + "]").c_str());
	return 0;
}

void plot_dc() {
	plot_dc("./sample/dc-002.lst", "./sample/dc-002.pdf");
}
